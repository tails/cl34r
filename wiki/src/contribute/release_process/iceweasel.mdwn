[[!meta title="Releasing Iceweasel + Torbrowser patches"]]

[[!toc levels=2]]

1. Prepare environment
======================

* Clone the Tails browser [[Git
  repository|contribute/git#other-repositories]] if you do not have it
  handy yet.

* Add (and fetch from) a Git remote for the Debian iceweasel packaging
  repository:

      git remote add -f debian git://git.debian.org/git/pkg-mozilla/iceweasel.git

* Export the new upstream release to the environment of the one shell
  or three that will be used:

	export VERSION=17.0.9esr

2. Was Iceweasel updated?
=========================

It might have been updated in one of these sources:

* branch `esr17/master` in `git://git.debian.org/git/pkg-mozilla/iceweasel.git`
* <http://mozilla.debian.net/pool/iceweasel-esr/i/iceweasel/>

**If** it was updated, then skip to [[New Iceweasel release|iceweasel#new-iceweasel-release]].
**Else**, skip to [[New Firefox release|iceweasel#new-firefox-release]].

<a id="new-firefox-release"></a>

3. New Firefox release
======================

If Iceweasel was not updated to match the new Firefox release we want,
a bit more work is needed.

Note that usually, we're doing these steps (usually on Sunday or
Monday) *before* the new ESR was officially released (which usually
happens on Tuesday). Mozilla make the source available on previous
Friday or Saturday, so that downstreams (such as us!) can get their stuff ready
in time for the security announce.

* Download the Firefox tarball and detached signature from
  <https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/VERSION/source/>
  (`VERSION` is the version we want to build, that is something like `17.0.7esr`).
* Check the signature.
* Put the tarball in the parent directory of your Iceweasel Git repository.
* Extract the tarball.
* `cd` into the extracted directory.
* Copy the `debian/` directory from our previous package into the new
  upstream source directory.
* Use `dch` to add a `debian/changelog` entry matching the new
  upstream version. Use 0 for the Debian packaging version, e.g.
  `17.0.5esr-0`, to leave room for the official packaging that we will
  want to merge when it's out:

      dch -v ${VERSION}-0 "New upstream release."

* Download and repack the other tarballs:

      make -f debian/rules download

* `cd` into our Iceweasel Git directory.
* Checkout the `tails/master` branch.
* Get yourself a new upstream branch:

      git branch -D upstream
      git branch upstream tails/master

* Trick the tarball importer to import the correct version:

      cp ../mozilla-esr17/browser/config/version.txt browser/config/ && \
      cp ../mozilla-esr17/debian/changelog debian/

* Import the new upstream release into the `upstream` branch:

      make -f debian/rules import

* Merge the import commit into `tails/master`:

      git reset --hard && git merge upstream

* Get the `debian` directory back:

      git checkout HEAD^ -- debian
      git commit -m 'Get Debian packaging directory back.'

* Cleanup quilt status:

      rm -rf .pc

* Apply all quilt patches:

      quilt push -a

* `git rm` files deleted by quilt patches

* `git add` files created by quilt patches

* Commit:

      git commit -a -m 'Apply all quilt patches.'

<a id="new-iceweasel-release"></a>

4. New Iceweasel release
=========================

Skip this entire stage if you imported a new Firefox release.

The way to proceed is different depending on whether Debian's
iceweasel was pushed to it yet, or not.

If Debian's iceweasel was pushed to Git already
-----------------------------------------------

* Retrieve the update from the iceweasel Git repository and verify the
  Git tag you want to import, e.g.

      git fetch debian && git tag -v debian/17.0.8esr-1

* Checkout our `tails/master` branch.

* Unapply all Torbrowser patches:
  - If quilt knows they are applied (`quilt applied` will tell you),
    then use `quilt pop` as many times as needed.
  - Else, some manual care is needed so that quilt internal state
    matches the actual state of the source tree. We need to manually
    unapply all quilt patches, then reapply them all:

        for p in $(tac debian/patches/series) ; do
           patch -p1 -R < "debian/patches/$p"
        done && quilt push -a

    ... and then use `quilt pop` as many times as needed to unapply
    all Torbrowser patches.

* `git add` the new files and the modified ones

* `git rm` the deleted files

* Commit:

      git commit -m 'Remove Torbrowser patches.'

* Merge the tag, e.g.

      git merge debian/17.0.8esr-1

* Verify with that `tails/master` is in the same state as Debian's
  iceweasel, e.g.

      git diff --stat debian/17.0.8esr-1..tails/master

  All expected differences should be in `debian/{changelog,rules}`,
  and the addition of the Torbrowser patches.

If Debian's iceweasel was not pushed to Git yet
-----------------------------------------------

FIXME: test these instructions.

Then, we have to import the source package into Git ourselves, and
merge from Debian's Vcs-Git later.

* Download, verify and extract the new iceweasel source package with dget.

* Checkout our `tails/master` branch.

* Overwrite the files in the Git checkout with the new ones.
  Assuming the new extracted iceweasel package is in
  `iceweasel-17.0.2esr`, and our iceweasel Git repository checkout is
  in `git`:

      rsync --stats -a --exclude=.git --delete iceweasel-17.0.2esr/ git/

* `git add` the new files and the modified ones

* `git rm` the deleted files

* Commit.

* Verify with `diff` that the current state of the `tails/master` is
  exactly the same as Debian's iceweasel source package one.

* Bring our `debian/changelog` and `debian/rules` changes back.

5. Update Torbrowser patches
============================

First, check if the Torbrowser patches were updated since the last
time we imported them (that's why we always record in
`debian/changelog` the TBB Git commit we are importing from).

**If** the Torbrowser patches were not updated, then just apply them
and commit:

    quilt push -a && git commit -a -m 'Apply Torbrowser patches.'

... then skip this entire stage.

**Else**, proceed with the following steps.

* Make sure all quilt patches are applied.
* Unapply all Torbrowser patches: use `quilt pop` as many times
  as needed.
* Remove all Torbrowser patches from the series:

      quilt unapplied | xargs -n 1 quilt delete

* Remove Torbrowser patches from Git:

      git rm -r debian/patches/torbrowser/

* Commit:

      git commit -a -m 'Remove Torbrowser patches.'

* Import the latest TBB patches:

      export TBB_SRC=XXX_PATH_TO_TBB_SRC_XXX
      for patch in $(\ls --reverse ${TBB_SRC}/src/current-patches/firefox/*.patch) ; do
        p=$(basename "$patch")
        quilt import -P "torbrowser/$p" "$patch"
      done
      git add debian/patches/torbrowser debian/patches/series
      TBB_COMMIT=$(git --git-dir=$TBB_SRC/.git rev-parse HEAD)
      git commit -a -m "Import Torbrowser patches at commit ${TBB_COMMIT}."

* Remove from the quilt series, using `quilt delete`, the Torbrowser
  patches we don't want: see `debian/changelog` for the list of
  patches skipped last time, see the TBB Git log to make your opinion
  about new or updated patches, use common sense. Commit with
  a message explaining your decisions.

* Apply Torbrowser patches:

      quilt push -a && git commit -a -m 'Apply Torbrowser patches.'

6. Build packages
=================

Update debian/changelog
-----------------------

* set a version such as `17.0.5esr-0+tails1`, e.g.

      dch -v "${VERSION}-0+tails1"

* list our changes, especially the TBB commit at which the
  patches were imported, and the ones we skipped
* set distribution to unstable
* commit:

      git commit debian/changelog \
          -m "$(head -n 1 debian/changelog | sed -e 's,).*,),')"

Tag the release
---------------

	git tag -s -m "$(head -n 1 debian/changelog | sed -e 's,).*,),')" "debian/${VERSION}-0+tails1"

Build for unstable
------------------

If you have no available non-Tails setup to comfortably test these
packages, then skip this step.

* Build for unstable and the architecture you can test on (most likely
  amd64), e.g. using our [[contribute/Debian_package_builder]].
* Install and test the resulting packages.

Build for squeeze-backports
---------------------------

* Checkout the `tails/squeeze` branch.
* Merge the tag.
* Add a squeeze-backport changelog entry:

      dch --bpo

  Adjust as needed.

* Commit:

      git commit debian/changelog \
          -m "$(head -n 1 debian/changelog | sed -e 's,).*,),')"

* Tag the backport:

      git tag -s -m "$(head -n 1 debian/changelog | sed -e 's,).*,),')" $TAG_NAME

* Build for squeeze-backports and i386, e.g. using our
  [[contribute/Debian_package_builder]].
* Integrate these debs into your apt-cacher cache. That's one cp to
  `/var/cache/apt-cacher-ng/_import/` away, + 1 click in the
  web interface.
* Test the resulting packages in Tails.
* Make sure the `.orig.*` tarballs are included in the `.changes`
  file. FIXME: check if that's needed, or done automatically by the
  above instructions.
* Upload the resulting packages to the relevant suite of our
  [[contribute/APT repository]].
* Merge this APT suite where you need it: generally, that's `devel`,
  `experimental`, one of `stable` or `testing`, and maybe
  a release tag.

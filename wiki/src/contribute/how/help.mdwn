[[!meta title="Help other Tails users"]]

Hanging out on [[our IRC channel|contribute/talk]] and
providing assistance to new users is incredibly valuable.

Some "public" wi-fi hot spots redirect a web browser to an initial starting page that requires "Acceptance" of the "Terms of Use." Some of these hot spots are password-protected; some are not.

Regardless, TAILS is always stuck in the "synchorizing-the-clock" mode after connection has been established to these sorts of networks. Is there any way to overcome this hurdle when connecting to the Internet?

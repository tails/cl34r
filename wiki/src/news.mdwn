[[!meta title="News"]]

<div id="tor_check">
<a href="https://check.torproject.org/">
[[!img "lib/onion.png" link="no"]]
<span>Tor check</span>
</a>
</div>

To be informed of the latest versions of Tails and follow the life of the
project you can:

  - subscribe to the RSS feed of this page
  - subscribe to the [amnesia-news
    mailing list](https://boum.org/mailman/listinfo/amnesia-news) where
    the same news are sent by email:
    <form method="POST" action="https://mailman.boum.org/subscribe/amnesia-news">
            <input class="text" name="email" value=""/>
            <input class="button" type="submit" value="Subscribe"/>
    </form>
  - follow us on Twitter [@Tails_live](http://twitter.com/tails_live).

[[!inline pages="news/* and !news/*/* and !news/discussion and currentlang()" show="10"]]
[[!inline pages="news/* and !news/*/* and !news/discussion and currentlang() and tagged(announce)"
          show="10" feeds="yes" feedonly="yes" feedfile="emails"]]

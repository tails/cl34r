Fixed [[!tails_todo "" desc="todo"]] items are tagged `pending` when they are
fixed in Git, but not in any stable release yet.

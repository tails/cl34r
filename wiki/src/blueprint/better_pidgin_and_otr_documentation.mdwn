The [[documentation about Pidgin and OTR|doc/anonymous_internet/pidgin/]] should be improved.

Pidgin
======

- [[todo/mention_IRC_channel_in_Pidgin_documentation]]
- [[todo/document_the_random_nick_preset]]

OTR
===

- [[todo/better_link_to_the_OTR_homepage]]
- [[todo/integrate_with_external_OTR_documentation]]

[[!tag priority/normal]]

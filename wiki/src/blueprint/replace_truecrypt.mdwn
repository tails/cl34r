Due to various concerns, Trecrypt is about to be replaced in Tails,
either by tcplay or cryptsetup.

# Candidate alternatives

## Tc-play

[tc-play](https://github.com/bwalex/tc-play) is a Free implementation
of TrueCrypt based on dm-crypt, licensed under the 2-clause BSD license.
It is in Debian sid ([[!debpts tcplay]]), and would serve as a full
replacement of TrueCrypt... once a proper GUI available.

## Cryptsetup

[Cryptsetup 1.6 supports reading the TrueCrypt
on-disk format](https://code.google.com/p/cryptsetup/wiki/Cryptsetup160),
so if/when udisks and friends are adapted (if needed), then we could
as well avoid shipping any additional software at all. It is part of
Debian unstable.

## Zulucrypt

[zuluCrypt](https://code.google.com/p/zulucrypt) is a front end to cryptsetup
and tcplay, it make easy to manage Truecrypt
volumes through a GUI, but it's not packaged in Debian yet
([[!debbug 703911 desc="RFP #703911"]]).

[[!tag release/2.0]]

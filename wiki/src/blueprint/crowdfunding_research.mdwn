[[!toc levels=1]]

# Things to look for

We should investigate the flexibility of how you can define the goals,
e.g. can you only have one, or can you have many? For the former we
probably would have to setup a separate campaign for each deliverable
(which may introduce more overhead), and but for the latter we could
just make each deliverable a sub-goal of the general "Fund Tails
development" campaign.

We should also investigate the financial overhead (e.g. fees) of these
options.

Here's a useful report about crowfunding:
<http://whois--x.net/x-net-publica-informe-sobre-situacion-crowdfunding>
The interesting bit is this table comparing different sites (page 8).

# Candidates to investigate

* http://www.fundageek.com
* http://pledgie.com
* http://kickstarter.com
* http://www.indiegogo.com
* Joey's DYI solution: https://campaign.joeyh.name

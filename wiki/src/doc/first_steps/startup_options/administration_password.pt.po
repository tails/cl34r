# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-06-13 16:34+0300\n"
"PO-Revision-Date: 2012-07-20 19:36+0100\n"
"Last-Translator: drebs <drebs@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Setting an administration password\"]]\n"
msgstr "[[!meta title=\"Configurando uma senha de administração\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"In Tails, an administration password is required to perform system\n"
"administration tasks.<br/>\n"
"For example:\n"
msgstr ""
"No Tails, uma senha de administração é necessária para realizar\n"
"tarefas de administração de sistema.<br />\n"
"Por exemplo:\n"

#. type: Bullet: '  - '
msgid "To install new programs and packages"
msgstr "Para instalar novos programas e pacotes"

#. type: Bullet: '  - '
msgid "To access the internal hard disks of the computer"
msgstr "Para acessar os discos rígidos internos do computador"

#. type: Bullet: '  - '
msgid "To execute commands with <span class=\"command\">sudo</span>"
msgstr "Para execuar comandos com <span class=\"command\">sudo</span>"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "**By default, the administration password is disabled for better security.**\n"
#| "This can prevent an attacker with physical or remote access to your Tails system\n"
#| "to gain administration privileges and perform administration task without your\n"
#| "knowledge.\n"
msgid ""
"**By default, the administration password is disabled for better security.**\n"
"This can prevent an attacker with physical or remote access to your Tails system\n"
"to gain administration privileges and perform administration tasks\n"
"against your will.\n"
msgstr ""
"**Por padrão, a senha de administração é desabilitada para maior segurança.**\n"
"Esto pode que um atacante com acesso físico ou remoto ao seu sistema Tails\n"
"ganhe privilégios administrativos e execute tarefas de administração sem seu\n"
"conhecimento.\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "In order to perform administration tasks, you need to setup an administration\n"
#| "password when starting Tails, using [[<span class=\"application\">Tails\n"
#| "greeter</span>|startup_options#tails_greeter]].\n"
msgid ""
"In order to perform administration tasks, you need to set up an administration\n"
"password when starting Tails, using [[<span class=\"application\">Tails\n"
"greeter</span>|startup_options#tails_greeter]].\n"
msgstr ""
"Para poder realizar tarefas de administração, você precisa configurar uma senha\n"
"de administração ao iniciar o Tails, usando o [[<span class=\"application\">Tails\n"
"greeter</span>|startup_options#tails_greeter]].\n"

#. type: Plain text
#, no-wrap
msgid ""
"1.  When <span class=\"application\">Tails greeter</span> appears, in the\n"
"<span class=\"guilabel\">Welcome to Tails</span> window, click on the\n"
"<span class=\"button\">Yes</span> button. Then click on the\n"
"<span class=\"button\">Forward</span> button to switch to the\n"
"<span class=\"guilabel\">Administration password</span> window.\n"
msgstr ""
"1.  Quando o <span class=\"application\">Tails greeter</span> aparecer, na janela\n"
"<span class=\"guilabel\">Bem vindo/a ao Tails</span>, clique no botão\n"
"<span class=\"button\">Sim</span>. Em seguida clique no botão\n"
"<span class=\"button\">Próximo</span> para acessar a janela de\n"
"<span class=\"guilabel\">Senha administrativa</span>.\n"

#. type: Plain text
#, no-wrap
msgid ""
"2.  In the <span class=\"guilabel\">Administration password</span> window, specify\n"
"a password of your choice in both the <span class=\"guilabel\">Password</span> and\n"
"<span class=\"guilabel\">Verify Password</span> text boxes. Then click on the\n"
"<span class=\"button\">Login</span> button to start the\n"
"<span class=\"application\">GNOME Desktop</span>.\n"
msgstr ""
"2.  Na janela de <span class=\"guilabel\">Senha administrativa</span>, especifique\n"
"uma senha de sua escolha nas caixas <span class=\"guilabel\">Senha</span> e\n"
"<span class=\"guilabel\">Verifique a senha</span>. Em seguida clique no botão\n"
"<span class=\"button\">Login</span> para iniciar o \n"
"<span class=\"application\">GNOME Desktop</span>.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"open_root_terminal\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "How to open a root terminal\n"
msgstr ""

#. type: Plain text
msgid "To open a root terminal, you can do any of the following:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  - Choose\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">Accessories</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">Root Terminal</span></span>.\n"
msgstr ""

#. type: Bullet: '  - '
#, fuzzy
#| msgid "To execute commands with <span class=\"command\">sudo</span>"
msgid "Execute <span class=\"command\">sudo -i</span> in a terminal."
msgstr "Para execuar comandos com <span class=\"command\">sudo</span>"

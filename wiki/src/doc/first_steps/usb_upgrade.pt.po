# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-01-02 00:35+0100\n"
"PO-Revision-Date: 2012-07-11 15:21+0100\n"
"Last-Translator: drebs <drebs@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Upgrading a Tails USB Stick\"]]\n"
msgstr "[[!meta title=\"Atualizando uma memória USB com Tails\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"<span class=\"application\">Tails USB installer</span> also allows you to upgrade\n"
"a USB stick to a newer version of Tails.\n"
msgstr ""
"O <span class=\"application\">Instalador USB do Tails</span> também permite fazer a atualização\n"
"da memória USB para uma versão mais nova do Tails.\n"

#. type: Plain text
#, no-wrap
msgid ""
"The following techniques only work if the USB stick was installed using\n"
"<span class=\"application\">Tails USB installer</span>. **The persistent storage\n"
"on the USB stick will be preserved.** There are two methods to do the upgrade:\n"
msgstr ""
"A técnica a seguir somente funciona se a memória USB foi instalada usando\n"
"o <span class=\"application\">Instalador USB do Tails</span>. **O armazenamento\n"
"persistente na memória USB será preservado.** Existem dois métodos para realizar a atualização:\n"

#. type: Bullet: '  - '
msgid ""
"[[Upgrade by cloning from another USB stick|usb_upgrade#clone]] which "
"already runs a newer version of Tails"
msgstr ""
"[[Atualizar clonando de uma outra memória USB|usb_upgrade#clone]] que já "
"contenha uma versão nova do Tails"

#. type: Bullet: '  - '
msgid ""
"[[Upgrade from an ISO image|usb_upgrade#from_iso]] of a newer version of "
"Tails"
msgstr ""
"[[Atualizar de uma imagem ISO|usb_upgrade#from_iso]] de uma versão mais nova "
"do Tails"

#. type: Plain text
#, no-wrap
msgid ""
"As for the installation, you need to start\n"
"<span class=\"application\">Tails USB installer</span> from another media than the\n"
"USB stick that you want to upgrade.\n"
msgstr ""
"Para a instalação, você precisa iniciar o\n"
"<span class=\"application\">Instalador USB do Tails</span> de uma outra midia que não seja amemória USB que você quer atualizar.\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"clone\"></a>\n"
msgstr "<a id=\"clone\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Clone & Upgrade\n"
msgstr "Clonar & Atualizar\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Start Tails, version 0.11 or later, from the USB that you want to clone "
#| "from."
msgid "Start Tails from the USB that you want to clone from."
msgstr ""
"Inicie o Tails, versão 0.11 ou maior, a partir do dispositivo USB que você "
"quer clonar."

#. type: Plain text
#, no-wrap
msgid ""
"2. Choose\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"     <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Tails USB installer</span>\n"
"   </span>\n"
"   to start <span class=\"application\">Tails USB installer</span>.\n"
msgstr ""
"2. Escolha\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Aplicações</span>&nbsp;▸\n"
"     <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Instalador USB do Tails</span>\n"
"   </span>\n"
"   para iniciar o <span class=\"application\">Instalador USB do Tails</span>.\n"

#. type: Bullet: '3. '
msgid "Choose <span class=\"guilabel\">Clone & Upgrade</span>."
msgstr "Escolha <span class=\"guilabel\">Clonar & Atualizar</span>."

#. type: Bullet: '4. '
msgid "Plug the USB stick that you want to upgrade."
msgstr "Conecte a memória USB que você quer atualizar."

#. type: Plain text
#, no-wrap
msgid ""
"   A new device, which corresponds to the USB stick, appears in the\n"
"   <span class=\"guilabel\">Target Device</span> drop-down list.\n"
msgstr ""
"   Um novo dispositivo, que corresponde à memória USB, aparecerá na\n"
"   lista de opções <span class=\"guilabel\">Dispositivos de Destino</span>.\n"

#. type: Bullet: '5. '
msgid ""
"Choose the USB stick from the <span class=\"guilabel\">Target Device</span> "
"drop-down list."
msgstr ""
"Escolha a memória USB a partir da lista de opções <span class=\"guilabel"
"\">Dispositivos de Destino</span>."

#. type: Bullet: '6. '
msgid ""
"To start the upgrade, click on the <span class=\"button\">Create Live USB</"
"span> button."
msgstr ""
"Para iniciar a atualização, clique no botão <span class=\"button\">Criar USB "
"Live</span>."

#. type: Bullet: '7. '
msgid ""
"Read the warning message in the text area. Click on the <span class=\"button"
"\">Next</span> button to confirm."
msgstr ""
"Leia a mensagem de advertência na área de texto. Clique no botão <span class="
"\"button\">Próximo</span> para confirmar."

#. type: Plain text
#, no-wrap
msgid "<a id=\"from_iso\"></a>\n"
msgstr "<a id=\"from_iso\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Upgrade from ISO\n"
msgstr "Atualize a partir de uma imagem ISO\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Start Tails, version 0.11 or later, from another media than the USB stick "
#| "that you want to upgrade."
msgid ""
"Start Tails from another media than the USB stick that you want to upgrade."
msgstr ""
"Inicie o Tails, versão 0.11 ou maior, a partir de uma midia que não seja a "
"memória USB que você quer atualizar."

#. type: Bullet: '3. '
msgid "Choose <span class=\"guilabel\">Upgrade from ISO</span>."
msgstr ""
"Escolha <span class=\"guilabel\">Atualizar a partir da imagem ISO</span>."

#. type: Bullet: '6. '
msgid ""
"Click on the <span class=\"guilabel\">Browse</span> button to specify the "
"location of the ISO image."
msgstr ""
"Clique no botão <span class=\"guilabel\">Browse</span> para especificar a "
"localização da imagem ISO."

#. type: Plain text
#, no-wrap
msgid ""
"   If the ISO image is saved on another media, plug it if necessary and click on\n"
"   the corresponding device in the <span class=\"guilabel\">Places</span> column.\n"
msgstr ""
"   Se a imagem ISO estiver salva em uma outra mídia, conecte-a se necessário e clique\n"
"   no dispositivo correspondente na coluna <span class=\"guilabel\">Locais</span>.\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"tip\">\n"
"   If the ISO image is stored in a persistent volume, the corresponding device\n"
"   appears first as <span class=\"emphasis\">Encrypted</span>. Click on the device\n"
"   and, in the popup window, enter the passphrase to unlock it.\n"
"   </div>\n"
msgstr ""
"   <div class=\"tip\">\n"
"   Se a imagem ISO estiver armazenada em um volume persistente, o dispositivo correspondente\n"
"   aparecerá primeiro como <span class=\"emphasis\">Criptografado</span>. Clique no dispositivo e,\n"
"   na janela que aparecerá, digite a senha para abrí-lo.\n"
"   </div>\n"

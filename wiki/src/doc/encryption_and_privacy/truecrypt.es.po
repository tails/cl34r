# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-03-13 14:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"TrueCrypt\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Security considerations\n"
msgstr ""

#. type: Plain text
msgid ""
"Although *TrueCrypt* looks like free software, [concerns](http://www."
"happyassassin.net/2008/08/06/open-letter-to-the-developers-of-truecrypt/) "
"over [its licence](http://www.truecrypt.org/legal/license) prevent its "
"inclusion in Debian.  Truecrypt is also *developed* in a closed fashion, so "
"while the source code is freely available, it may receive less review than "
"might a comparable openly developed project."
msgstr ""

#. type: Plain text
msgid ""
"For the above reasons, *Tails* developers do not recommend *TrueCrypt*.  We "
"include *TrueCrypt* only to allow users of the (old and now unsupported) "
"*Incognito* live system to access the data on previously created media."
msgstr ""

#. type: Plain text
msgid ""
"In the future, we would like to [[!tails_todo "
"provide_a_migration_path_from_truecrypt desc=\"provide proper alternatives"
"\"]] and stop distributing *TrueCrypt*. This means that you should **not** "
"create new TrueCrypt media if you intend to stay with Tails in the long run."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Using TrueCrypt in Tails\n"
"==========================\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"*TrueCrypt* is not enabled by default when Tails starts. In order to use\n"
"*TrueCrypt*, add the <span class=\"command\">truecrypt</span> boot option\n"
"to the <span class=\"application\">boot menu</span>. For detailed\n"
"instructions, see the documentation on [[using the <span\n"
"class=\"application\">boot\n"
"menu</span>|first_steps/startup_options#boot_menu]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Once Tails has started, to start <span\n"
"class=\"application\">TrueCrypt</span> choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Accessories</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">TrueCrypt</span></span>.\n"
msgstr ""
